extends TileMap

const EMPTY_TILE = -1
const SIMPLE_TILE = 0
const DOUBLE_TILE = 1
const POWER_UP_TILE = 4

const PowerUp = preload("res://src/powerUp/PowerUp.tscn")
const BrickAnimation = preload("res://src/animations/BrickAnimation.tscn")
const levelBorders = Rect2(1, 2, 17, 14)

signal no_bricks

var tiles_count: int = 0 setget set_tiles_count


func _ready():
	generate_level(1)
	GlobalStatus.connect("next_level", self, "generate_level")


func generate_level(level: int):
	print("generate_level")
	randomize()
	var tiles_probabilities = {
		EMPTY_TILE: (15 / level)*30 ,
		SIMPLE_TILE: 50 / level,
		DOUBLE_TILE: 10,
		POWER_UP_TILE: max(3, 25 / level)
	}
	var total = 0
	for i in tiles_probabilities.keys():
		total += tiles_probabilities[i]

	for x in range(levelBorders.size.x):
		for y in range(levelBorders.size.y):
			var random_tile = randi() % total
			var offset = 0
			for i in tiles_probabilities.keys():
				if random_tile < offset + tiles_probabilities[i]:
					set_cellv(Vector2(levelBorders.position.x + x, levelBorders.position.y + y), i)
					break
				else:
					offset += tiles_probabilities[i]

	self.tiles_count = len(get_used_cells())


func hit(collision: KinematicCollision2D) -> void:
	var tile_position: Vector2 = collision.position - collision.normal
	if hit_tile(tile_position):
		return
	if hit_tile(tile_position + Vector2(1, 0)):
		return
	if hit_tile(tile_position + Vector2(-1, 0)):
		return
	if hit_tile(tile_position + Vector2(0, 1)):
		return
	if hit_tile(tile_position + Vector2(0, -1)):
		return


func hit_tile(world_position: Vector2) -> bool:
	var tile_position: Vector2 = world_to_map(world_position)

	match get_cellv(tile_position):
		EMPTY_TILE:
			return false
		SIMPLE_TILE:
			create_animation(tile_position, SIMPLE_TILE)
			set_cellv(tile_position, EMPTY_TILE)
			self.tiles_count -= 1
			GlobalStatus.score += 1
			return true
		DOUBLE_TILE:
			create_animation(tile_position, DOUBLE_TILE)
			set_cellv(tile_position, SIMPLE_TILE)
			GlobalStatus.score += 5
			return true
		POWER_UP_TILE:
			create_animation(tile_position, POWER_UP_TILE)
			set_cellv(tile_position, EMPTY_TILE)
			GlobalStatus.score += 10
			self.tiles_count -= 1
			create_power_up(tile_position)
			return true
	return false


func create_animation(tile_position: Vector2, type: int) -> void:
	var brickAnimation = BrickAnimation.instance()
	brickAnimation.global_position = map_to_world(tile_position)
	brickAnimation.call_deferred("play", type)
	get_parent().add_child(brickAnimation)


func create_power_up(tile_position: Vector2) -> void:
	var powerUp = PowerUp.instance()
	powerUp.global_position = map_to_world(tile_position)
	powerUp.call_deferred("set_random_type")
	get_parent().add_child(powerUp)


func set_tiles_count(count: int) -> void:
	tiles_count = count
	print("set_tiles_count ", tiles_count)
	if tiles_count <= 0:
		GlobalStatus.next_level()
