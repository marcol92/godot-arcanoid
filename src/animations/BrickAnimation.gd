extends Node2D

onready var animationPlayer = $AnimationPlayer


func play(type):
	match type:
		0:
			animationPlayer.play("SingleBrick")
		1:
			animationPlayer.play("DoubleBrick")
		4:
			animationPlayer.play("PowerUpBrick")


func animation_completed():
	queue_free()
