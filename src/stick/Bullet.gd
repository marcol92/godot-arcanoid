extends KinematicBody2D

# Properties
export (float) var MAX_SPEED = 60

func _physics_process(delta: float) -> void:
	var collision: KinematicCollision2D = move_and_collide(Vector2.UP * MAX_SPEED * delta)

	if collision:
		print("Collision with: ", collision.collider.name)
		if collision.collider.has_method("hit"):
			collision.collider.hit(collision)
		die()


func die():
	queue_free()
