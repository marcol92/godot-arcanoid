extends KinematicBody2D

# Properties
export (int) var MAX_SPEED = 150
export (int) var ACCELERATION = MAX_SPEED / .1  # seconds to get max speed
export (int) var FRICTION = MAX_SPEED / .05  # seconds to stop

const Bullet = preload("res://src/stick/Bullet.tscn")
# Status
enum Status { IDLE, MOVE }

var initial_position: Vector2 = Vector2.ZERO
var velocity: Vector2 = Vector2.ZERO
var status = Status.IDLE
var is_shooting: bool = false

# Nodes
onready var spriteL = $SpriteLeft
onready var spriteC = $SpriteCenter
onready var spriteR = $SpriteRight
onready var collisionL = $CollisionLeft
onready var collisionC = $CollisionCenter
onready var collisionR = $CollisionRight
onready var cannonL = $CannonLeft
onready var cannonR = $CannonRight
onready var animationPlayer = $AnimationPlayer


func _ready() -> void:
	initial_position = position
	GlobalStatus.connect("bullets_change", self, "bullets_change")
	GlobalStatus.connect("stick_length_change", self, "stick_length_change")
	GlobalStatus.connect("reset_level", self, "reset_position")


func _physics_process(delta: float) -> void:
	var input_vector: Vector2 = Vector2.ZERO
	if not GlobalStatus.game_pause:
		input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")

	if input_vector != Vector2.ZERO:
		status = Status.MOVE
		velocity = velocity.move_toward(input_vector * MAX_SPEED, ACCELERATION * delta)
	else:
		status = Status.IDLE
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)

	move(velocity, delta)

	if Input.is_action_just_pressed("attack"):
		if GlobalStatus.game_pause:
			GlobalStatus.game_pause = false
		if GlobalStatus.bullets > 0 and (not is_shooting):
			cannon_start_shooting()


func move(new_velocity: Vector2, delta: float) -> void:
	move_and_collide(new_velocity * delta)
	position.y = initial_position.y


func reset_position():
	position = initial_position


func get_velocity_effect() -> float:
	if status == Status.MOVE:
		return velocity.x
	return 0.0


func stick_length_change(new_length: float) -> void:
	# Sprite
	spriteC.scale.x = new_length
	var half_length: float = (new_length - 1) / 2.0
	spriteL.position.x = -16 * (1 + half_length)
	spriteR.position.x = 16 * (1 + half_length)
	# Collisions
	(collisionC.shape as RectangleShape2D).extents.x = 12 + 16 * half_length
	collisionL.position.x = -16 * half_length
	collisionR.position.x = 16 * half_length
	# Cannons
	cannonL.position.x = -16 * (1 + half_length)
	cannonR.position.x = 16 * (1 + half_length)


func bullets_change(bullets: int) -> void:
	if bullets > 0:
		cannonL.visible = true
		cannonR.visible = true
	else:
		cannonL.visible = false
		cannonR.visible = false


func cannon_start_shooting():
	animationPlayer.play("CannonsShoot")
	is_shooting = true


func generate_bullets() -> void:
	print("cannon_shoot: ", GlobalStatus.bullets)
	GlobalStatus.bullets -= 1
	var bulletL = Bullet.instance()
	var bulletR = Bullet.instance()
	bulletL.position = cannonL.global_position
	bulletR.position = cannonR.global_position
	bulletL.position.y -= 5
	bulletR.position.y -= 5
	get_parent().add_child(bulletL)
	get_parent().add_child(bulletR)


func cannon_shoot_animation_completed():
	generate_bullets()
	is_shooting = false
