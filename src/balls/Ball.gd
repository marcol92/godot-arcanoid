extends KinematicBody2D

# Properties
export (int) var INITIAL_SPEED = 80
export (int) var MAX_SPEED = 250
export (int) var SPEED_INCREMENT = 2
export (int) var SPEED_INCREMENT_FREQUENCY = 5
export (float) var VELOCITY_EFFECT_FACTOR = .2

# Status
var initial_position: Vector2 = Vector2.ZERO
var velocity: Vector2 = Vector2.ZERO
var speed: float = INITIAL_SPEED

# Nodes
onready var speed_up_timer = $SpeedUpTimer
onready var power_ball_timer = $PowerBallTimer
onready var sprite = $Sprite
onready var audioStreamPlayer = $AudioStreamPlayer


func _ready() -> void:
	initial_position = position
	randomize()
	speed_up_timer.start(SPEED_INCREMENT_FREQUENCY)
	reset()
	GlobalStatus.connect("power_ball_change", self, "power_ball_change")
	GlobalStatus.connect("reset_level", self, "reset")


func _physics_process(delta: float) -> void:
	if not GlobalStatus.game_pause:
		velocity = velocity.normalized() * speed
		var collision: KinematicCollision2D = move_and_collide(velocity * delta)

		if collision:
			# print("Collision with: ", collision.collider.name)
			if collision.collider.has_method("hit"):
				# Collision with tile
				if not GlobalStatus.power_ball:
					audioStreamPlayer.play()
					velocity = velocity.bounce(collision.normal)
				collision.collider.hit(collision)
			elif collision.collider.has_method("get_velocity_effect"):
				# Collision with stick
				audioStreamPlayer.play()
				velocity = velocity.bounce(collision.normal)
				var velocity_effect: float = collision.collider.get_velocity_effect()
				velocity.x += velocity_effect * VELOCITY_EFFECT_FACTOR
			else:
				audioStreamPlayer.play()
				velocity = velocity.bounce(collision.normal)


func die() -> void:
	GlobalStatus.lifes -= 1

func reset():
	position = initial_position
	speed = INITIAL_SPEED
	velocity = Vector2.UP
	velocity.x = rand_range(-1.0, 1.0)

func _on_SpeedUpTimer_timeout():
	if not GlobalStatus.game_pause:
		speed = min(MAX_SPEED, speed + SPEED_INCREMENT)
	speed_up_timer.start()
	print("New speed: ", speed)


func power_ball_change(status):
	if status:
		power_ball_timer.start()
		sprite.region_rect = Rect2(0, 16, 16, 16)
	else:
		sprite.region_rect = Rect2(16, 16, 16, 16)


func _on_PowerBallTimer_timeout():
	GlobalStatus.power_ball = false
