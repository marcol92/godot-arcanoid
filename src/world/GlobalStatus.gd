extends Node
const CANNON_BULLETS = 10
const INITIAL_LIFES = 3
const MAX_STICK_LENGTH = 5

var lifes: int = INITIAL_LIFES setget set_lifes
var level: int = 1
var score: int = 0 setget set_score
var high_score: int = 0
var bullets: int = 0 setget set_bullets
var power_ball: bool = false setget set_power_ball
var stick_length: float = 1 setget set_stick_length
var game_pause: bool = true setget set_game_pause

signal stick_length_change(length)
signal power_ball_change(status)
signal bullets_change(bullets)
signal score_change(score)
signal lifes_change(lifes)
signal reset_level
signal next_level(level)
signal game_pause_change(status)


onready var clickAudio = $ClickAudio


func set_lifes(new_lifes: int) -> void:
	emit_signal("lifes_change", new_lifes)
	if new_lifes <= 0:
		high_score = max(high_score, score)
		get_tree().change_scene("res://src/levels/GameOver.tscn")

	if new_lifes < lifes:
		reset_level()

	lifes = new_lifes


func set_score(new_score: int) -> void:
	score = new_score
	emit_signal("score_change", score)


func set_game_pause(new_status: bool) -> void:
	game_pause = new_status
	emit_signal("game_pause_change", game_pause)


func set_stick_length(new_stick_length: float) -> void:
	stick_length = min(MAX_STICK_LENGTH, new_stick_length)
	emit_signal("stick_length_change", stick_length)


func set_bullets(new_bullets: int) -> void:
	bullets = new_bullets
	emit_signal("bullets_change", bullets)


func set_power_ball(new_status: bool) -> void:
	power_ball = new_status
	emit_signal("power_ball_change", power_ball)


func activate_cannons():
	self.bullets = CANNON_BULLETS


func reset_game():
	print("reset_game")
	self.score = 0
	self.level = 0
	self.lifes = INITIAL_LIFES
	next_level()


func reset_level():
	print("reset_level")
	self.bullets = 0
	self.power_ball = false
	self.stick_length = 1
	self.game_pause = true
	emit_signal("reset_level")


func next_level():
	print("next_level")
	reset_level()
	self.level += 1
	emit_signal("next_level", level)


func play_click():
	clickAudio.play()