extends CanvasLayer

onready var lifesLabel = $LifesLabel
onready var scoreLabel = $ScoreLabel
onready var bulletsLabel = $BulletsLabel
onready var cannonSprite = $CannonSprite
onready var gamePauseLabel = $GamePauseLabel


func _ready():
	set_lifes(GlobalStatus.lifes)
	GlobalStatus.connect("lifes_change", self, "set_lifes")
	GlobalStatus.connect("score_change", self, "set_score")
	GlobalStatus.connect("bullets_change", self, "set_bullets")
	GlobalStatus.connect("bullets_change", self, "set_bullets")
	GlobalStatus.connect("game_pause_change", self, "set_game_pause")


func set_lifes(lifes: int) -> void:
	lifesLabel.text = str(lifes)


func set_score(score: int) -> void:
	scoreLabel.text = str(score)


func set_bullets(bullets: int) -> void:
	bulletsLabel.text = str(bullets)
	if bullets > 0:
		bulletsLabel.visible = true
		cannonSprite.visible = true
	else:
		bulletsLabel.visible = false
		cannonSprite.visible = false


func set_game_pause(status: bool) -> void:
	gamePauseLabel.visible = status
