extends StaticBody2D

func _on_Area2D_area_entered(area: Area2D) -> void:
	if area.has_method("die"):
		area.die()

func _on_Area2D_body_entered(body: Node) -> void:
	if body.has_method("die"):
		body.die()
