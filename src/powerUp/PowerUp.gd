extends Area2D

# Properties
enum TypeEnum { LIFE_UP = 0, LENGTH_UP, POWER_BALL, CANNONS }
export (TypeEnum) var DEFAULT_TYPE = TypeEnum.LIFE_UP
export (float) var MAX_SPEED = 30
export (float) var LENGTH_UP_FACTOR = 0.5

const TypeRegion = {
	TypeEnum.LIFE_UP: Rect2(32, 0, 16, 16),
	TypeEnum.LENGTH_UP: Rect2(16, 0, 16, 16),
	TypeEnum.POWER_BALL: Rect2(48, 0, 16, 16),
	TypeEnum.CANNONS: Rect2(64, 0, 16, 16),
}

# Status
var type: int setget set_type

# Nodes
onready var sprite = $Sprite
onready var audioStreamPlayer = $AudioStreamPlayer


func _ready() -> void:
	randomize()
	self.type = DEFAULT_TYPE
	audioStreamPlayer.connect("finished", self, "audioStreamPlayer_finised")


func set_random_type():
	var type_list = [TypeEnum.LIFE_UP, TypeEnum.LENGTH_UP, TypeEnum.POWER_BALL, TypeEnum.CANNONS]
	type_list.shuffle()
	self.type = type_list.front()


func _physics_process(delta: float) -> void:
	position += Vector2.DOWN * delta * MAX_SPEED


func set_type(new_type: int) -> void:
	type = new_type
	sprite.region_rect = TypeRegion[type]


func die():
	queue_free()


func _on_PowerUp_body_entered(body: Node) -> void:
	if body.name == 'Stick':
		GlobalStatus.score += 100
		match type:
			TypeEnum.LIFE_UP:
				GlobalStatus.lifes += 1
			TypeEnum.LENGTH_UP:
				GlobalStatus.stick_length += LENGTH_UP_FACTOR
			TypeEnum.POWER_BALL:
				GlobalStatus.power_ball = true
			TypeEnum.CANNONS:
				GlobalStatus.activate_cannons()
		sprite.visible = false
		audioStreamPlayer.play()

func audioStreamPlayer_finised():
	queue_free()