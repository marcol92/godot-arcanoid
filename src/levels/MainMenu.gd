extends Control


func _on_PlayButton_pressed():
	GlobalStatus.call_deferred("reset_game")
	GlobalStatus.play_click()
	get_tree().change_scene("res://src/levels/Level.tscn")
