extends Control

onready var score_label = $ScoreLabel
onready var high_score_label = $HighScoreLabel


func _ready():
	score_label.text = 'Score: ' + str(GlobalStatus.score)
	high_score_label.text = 'High Score: ' + str(GlobalStatus.high_score)


func _on_RetryButton_pressed():
	GlobalStatus.reset_game()
	GlobalStatus.play_click()
	get_tree().change_scene("res://src/levels/Level.tscn")
